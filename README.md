# GitLab DAST

[![pipeline status](https://gitlab.com/gitlab-org/security-products/dast/badges/main/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/dast/commits/main)
[![coverage report](https://gitlab.com/gitlab-org/security-products/dast/badges/main/coverage.svg)](https://gitlab.com/gitlab-org/security-products/dast/commits/main)

GitLab tool for running Dynamic Application Security Testing (DAST) on provided
web site.

## How to use

1. Make your application or web site reachable on some URL. Example: `http://mysite.localhost`
1. `cd` into an empty directory, it will receive the JSON report file.
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume $(pwd)/wrk:/output:rw \
      --volume $(pwd)/wrk:/zap/wrk:rw \
      registry.gitlab.com/gitlab-org/security-products/dast:${VERSION:-latest} /analyze -t http://mysite.localhost -r report.html

    ```

    `VERSION` can be replaced with the latest available release matching your GitLab version. See [Versioning](#versioning-and-release-process) for more details.

1. The results will be displayed and also stored in `gl-dast-report.json`, you can view human-readable results in `report.html`
1. If you are running the scanner against localhost, you may need to add `--network host` to your `docker run` command to allow DAST to connect to your host machine's web server.

    To run an authenticated scan, run the Docker image with parameters:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume $(pwd)/wrk:/zap/wrk:rw \
      --volume $(pwd)/wrk:/output:rw \
      registry.gitlab.com/gitlab-org/security-products/dast:${VERSION:-latest} /analyze -t http://mysite.localhost/users/sign_in \
      --auth-url http://mysite.localhost/users/sign_in \
      --auth-username 'someone' \
      --auth-password 'p@ssw0rd' \
      --auth-username-field 'user[login]' \
      --auth-password-field 'user[password]' \
      --auth-exclude-urls 'http://mysite.localhost/logout_1,http://mysite.localhost/logout_2'
    ```

    **Caution:** `-t` option with the website address must always come before `--auth-*` options

## Development

### Build image

```sh
docker build -t dast .
```

### Run locally

To run DAST locally and perform a scan on a docker application listening on port 80 named `myapp`:

```sh
docker run -rm --name myapp -d myapp /some/adequate/startup/command
docker run -ti --rm --link myapp:myapp -v "$PWD":/output dast /analyze -t http://myapp
```

### Typing

DAST's Python code uses Python's [type hints](https://docs.python.org/3/library/typing.html).
The following rules must be followed for an MR to be approved. These rules only
apply to code in the `src` module. Tests do not need to be typed:

1. All new methods and classes MUST include type hints for their arguments and
    return values
1. Newly defined variables SHOULD have a type hint
1. Updated or refactored code without type hints SHOULD have type hints added

[mypy](http://mypy-lang.org/) is used for type linting and is installed with the other packages in
`requirements-test.txt`.

### Task execution tool

[Invoke](http://www.pyinvoke.org/) is used as a task execution tool for DAST. 
Example tasks include starting local servers for manual exploratory testing and printing metadata about ZAP a addon.

Available Tasks can be listed using `invoke --list`. Individual tasks can be executed using the task name, for example, `invoke zap.metadata --addon pscanrules`.

#### Task artifacts

Tasks that generate objects in the filesystem should store them in the
`artifacts/` directory. Its contents are already git-ignored to prevent
developers from having to clean up their filesystem after running a task.

### Tests

Instructions for testing can be found [here](doc/testing.md).

### Debugging locally

You can run DAST using a Docker container and have it point to an instance of ZAP running on your machine.
This allows you to watch the scan in the ZAP GUI as the scan takes place. Please see the [debug instructions](doc/debugging-locally.md) for more information.

### Read the ZAP database

Instructions for accessing the ZAP database can be found [here](doc/accessing-the-zap-database.md).

## Release process

Please check the [Release Process](doc/release-process.md) documentation.

### Versioning

Please check the [versioning](doc/versioning.md) documentation.

## Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).

## Support
For support on DAST visit https://about.gitlab.com/support/

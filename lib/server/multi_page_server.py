from .host import Host


class MultiPageServer:

    def __init__(self, run, port=80, secure_port=443):
        self.run = run
        self.port = port
        self.secure_port = secure_port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name multi_page_site '
                 f'-p {self.port}:80 '
                 '-v "${PWD}/test/end-to-end/fixtures/basic-multi-page-site":/usr/share/nginx/html:ro '
                 '-v "${PWD}/test/end-to-end/fixtures/basic-multi-page-site/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-v "${PWD}/test/unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt '
                 '-v "${PWD}/test/unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key '
                 '-d nginx:1.22.0')

        print(f'Multi-page site server started at http://{self.host.name()}:{self.port}/')
        return self

    def stop(self):
        self.run('docker rm --force multi_page_site >/dev/null 2>&1 || true')
        return self

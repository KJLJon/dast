#!/usr/bin/env bash

set -e

SCRIPTS_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/script-utils.sh"

# block the release if the variable is set
error_if_has_value "$BLOCK_RELEASE"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/changelog-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/release-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/docker-utils.sh"

log "Initializing environment"
verify_has_value "$GITLAB_API_TOKEN" "Aborting, environment variable GITLAB_API_TOKEN must contain a GitLab private token with access to this project."

PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/dast"}
VERSION="$(changelog_last_version)"
MAJOR=$(echo "$VERSION" | awk -F '.' '{print substr($1, 2)}')
MINOR=$(echo "$VERSION" | awk -F '.' '{print $2}')
PATCH=$(echo "$VERSION" | awk -F '.' '{print $3}')
CHANGELOG_DESCRIPTION=$(changelog_last_description)
RELEASE_DATA=$(build_release_json_payload "$VERSION" "$CHANGELOG_DESCRIPTION" "$PROJECT_URL" "$ALTERNATE_CI_REGISTRY_IMAGE" "$MAJOR" "$MINOR" "$PATCH")

log "Detected DAST $VERSION, verifying not already released"
verify_version_not_released "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION"

log "Releasing Docker images dast:$MAJOR.$MINOR.$PATCH to $CI_REGISTRY_IMAGE"
login_to_registry "gitlab-ci-token" "$CI_JOB_TOKEN" "$CI_REGISTRY"

# only need to pull the images once
DAST="$(dast_image 'built_image.txt')"
FUTURE_DAST="$(dast_image 'built_future_image.txt')"
DAST_FIPS="$(dast_image 'built_fips_image.txt')"
FUTURE_DAST_FIPS="$(dast_image 'built_future_fips_image.txt')"

pull_from_registry "$DAST"
pull_from_registry "$FUTURE_DAST"
pull_from_registry "$DAST_FIPS"
pull_from_registry "$FUTURE_DAST_FIPS"

push_dast_to_registry "$DAST" "$FUTURE_DAST" "$CI_REGISTRY_IMAGE" "$MAJOR" "$MINOR" "$PATCH" ""
push_dast_to_registry "$DAST_FIPS" "$FUTURE_DAST_FIPS" "$CI_REGISTRY_IMAGE" "$MAJOR" "$MINOR" "$PATCH" "-fips"

if [[ -n "$ALTERNATE_CI_REGISTRY_USERNAME" ]] && [[ -n "$ALTERNATE_CI_REGISTRY_PASSWORD" ]] && [[ -n "$ALTERNATE_CI_REGISTRY_IMAGE" ]]; then
  log "Releasing Docker images dast:$MAJOR.$MINOR.$PATCH to $ALTERNATE_CI_REGISTRY_IMAGE"
  push_dast_to_registry "$DAST" "$FUTURE_DAST" "$ALTERNATE_CI_REGISTRY_IMAGE" "$MAJOR" "$MINOR" "$PATCH" "" "$CI_REGISTRY" "gitlab-ci-token" "$CI_JOB_TOKEN" "$CI_REGISTRY" "$ALTERNATE_CI_REGISTRY_USERNAME" "$ALTERNATE_CI_REGISTRY_PASSWORD"
  push_dast_to_registry "$DAST_FIPS" "$FUTURE_DAST_FIPS" "$ALTERNATE_CI_REGISTRY_IMAGE" "$MAJOR" "$MINOR" "$PATCH" "-fips" "$CI_REGISTRY" "gitlab-ci-token" "$CI_JOB_TOKEN" "$CI_REGISTRY" "$ALTERNATE_CI_REGISTRY_USERNAME" "$ALTERNATE_CI_REGISTRY_PASSWORD"
fi

log "Tagging Git SHA $CI_COMMIT_SHA with $VERSION"
tag_git_commit "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION" "$CI_COMMIT_SHA"

log "Creating GitLab release from Git tag $VERSION"
create_gitlab_release "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$RELEASE_DATA"

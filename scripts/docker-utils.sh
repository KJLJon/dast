#!/usr/bin/env bash

set -e

PROJECT_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)/..")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/script-utils.sh"

# path_to_dast_image_name checks presence of and returns the path to the file containing the built DAST image
path_to_dast_image_name() {
  local image_file_name="$1"
  verify_has_value "$image_file_name" "Aborting, image file name has not been supplied to ${FUNCNAME[0]}"

  local image_file
  image_file=$(realpath "$PROJECT_DIR/$image_file_name")

  if ! [[ -f $image_file ]]; then
    error "Aborting, unable to determine previously built DAST image as file containing name doesn't exist: '$image_file'"
  fi

  echo "$image_file"
}

# dast_image returns the image name of the built DAST image
dast_image() {
  local image_file_name="$1"
  verify_has_value "$image_file_name" "Aborting, image file name has not been supplied to ${FUNCNAME[0]}"

  cat "$(path_to_dast_image_name "$image_file_name")"
}

# tag_and_push_image tags a docker image
# This will log into the from registry and into the to registry if credentials are supplied
# arguments: [from] [to] [from_registry] [from_username] [from_password] [to_registry] [to_username] [to_password]
tag_and_push_image() {
  local from="$1"
  local to="$2"

  # to registry credentials are optional, only required when pushing to an external registry
  local from_registry="$3"
  local from_username="$4"
  local from_password="$5"
  local to_registry="$6"
  local to_username="$7"
  local to_password="$8"

  verify_has_value "$from" "Aborting, unable to tag image as the from location has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$to" "Aborting, unable to tag image as the to location has not been supplied to ${FUNCNAME[0]}"

  if [[ "$from_username" != "" ]] && [[ "$from_password" != "" ]] && [[ "$from_registry" != "" ]]; then
    debug_log "from credentials supplied, authenticating to source registry"
    login_to_registry "$from_username" "$from_password" "$from_registry"
  fi

  pull_from_registry "$from"

  if [[ "$to_username" != "" ]] && [[ "$to_password" != "" ]] && [[ "$to_registry" != "" ]]; then
    debug_log "to credentials supplied, authenticating to destination registry"
    login_to_registry "$to_username" "$to_password" "$to_registry"
  fi

  debug_log "tagging from $from to $to"
  docker tag "$from" "$to"
  debug_log "pushing $to"
  docker push "$to"
}


# push_dast_to_registry pushes the DAST Docker image to a remote registry.
# Applies multiple tags to the same image.
# arguments: [DAST image] [DAST future image] [CI Registry Image] [Major Version] [Minor Version] [Patch Version] [Tag suffix] [from_registry] [from_username] [from_password] [to_registry] [to_username] [to_password]
push_dast_to_registry() {
  local dast="$1"
  local future_dast="$2"
  local ci_registry_image="$3"
  local major_version="$4"
  local minor_version="$5"
  local patch_version="$6"
  local suffix="$7"

  # registry credentials are optional, only required when pushing to an external registry
  local from_registry="$8"
  local from_username="$9"
  local from_password="${10}"
  local to_registry="${11}"
  local to_username="${12}"
  local to_password="${13}"

  verify_has_value "$dast" "Aborting, DAST image has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$future_dast" "Aborting, DAST future image has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$ci_registry_image" "Aborting, ci registry image has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$major_version" "Aborting, major version has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$minor_version" "Aborting, minor version has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$patch_version" "Aborting, patch version has not been supplied to ${FUNCNAME[0]}"

  tag_and_push_image "$dast" "$ci_registry_image:$major_version.$minor_version.$patch_version${suffix}" "$from_registry" "$from_username" "$from_password" "$to_registry" "$to_username" "$to_password"
  tag_and_push_image "$dast" "$ci_registry_image:$major_version.$minor_version${suffix}" "$from_registry" "$from_username" "$from_password" "$to_registry" "$to_username" "$to_password"
  tag_and_push_image "$dast" "$ci_registry_image:$major_version${suffix}" "$from_registry" "$from_username" "$from_password" "$to_registry" "$to_username" "$to_password"
  tag_and_push_image "$dast" "$ci_registry_image:latest${suffix}" "$from_registry" "$from_username" "$from_password" "$to_registry" "$to_username" "$to_password"
  tag_and_push_image "$future_dast" "$ci_registry_image:$((major_version + 1)).0.0-alpha${suffix}" "$from_registry" "$from_username" "$from_password" "$to_registry" "$to_username" "$to_password"
}

# pulls an image from the registry logs in to a remote registry
# arguments: [Image]
pull_from_registry() {
  local image_name="$1"

  verify_has_value "$image_name" "Aborting, image has not been supplied to ${FUNCNAME[0]}"

  if ! docker image inspect "$image_name" >/dev/null 2>/dev/null; then
    debug_log "pulling image from registry $image_name"
    docker pull "$image_name"
  fi
}

# login_to_registry logs in to a remote registry
# arguments: [Registry Username] [Registry Password] [CI Registry]
login_to_registry() {
  local registry_username="$1"
  local registry_password="$2"
  local ci_registry="$3"

  verify_has_value "$registry_username" "Aborting, ci registry username has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$registry_password" "Aborting, ci registry password has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$ci_registry" "Aborting, ci registry has not been supplied to ${FUNCNAME[0]}"

  debug_log "logging into $ci_registry"
  docker login -u "$registry_username" -p "$registry_password" "$ci_registry"
}

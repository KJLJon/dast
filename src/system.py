import pprint
import socket
import sys
from contextlib import closing
from datetime import datetime
from datetime import timezone
from subprocess import PIPE, Popen
from typing import List, Optional


class System:
    DAST_VERSION: Optional[str] = None

    def __init__(self):
        self.pretty_printer = pprint.PrettyPrinter(indent=4)

    def notify(self, message):
        print(message)

    def pretty_notify(self, obj):
        self.pretty_printer.pprint(obj)

    def dast_version(self) -> str:
        if not System.DAST_VERSION:
            commands = ['/usr/bin/bash', '-c', 'source /app/scripts/changelog-utils.sh && changelog_last_version']
            process = Popen(commands, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            output, err = process.communicate()

            output = output.decode('utf-8') if output else ''
            err = err.decode('utf-8') if err else ''
            System.DAST_VERSION = f'{output}{err}'.strip().replace('v', '')

        return System.DAST_VERSION

    def python_version(self) -> str:
        return ' '.join([x.strip() for x in sys.version.splitlines()])

    def sys_exit(self, exit_code):
        sys.exit(exit_code)

    def current_date_time(self):
        return datetime.now(timezone.utc)

    def get_free_port(self) -> int:
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            # Using port 0 asks the operating system for a free port
            if sock.connect_ex(('127.0.0.1', 0)) != 0:
                port = sock.getsockname()[1]
                return port

        raise RuntimeError('Failed to find free port')

    def run(self, parameters: List[str], output_file_name: str, **kwargs) -> Popen:
        with open(output_file_name, 'w') as out_file:
            return Popen(parameters, stdout=out_file, stderr=out_file, **kwargs)

    def run_piped_output(self, parameters: List[str], **kwargs) -> Popen:
        return Popen(parameters, stdout=PIPE, **kwargs)

import logging
from base64 import standard_b64encode
from os import linesep
from typing import Callable, Dict, List

from src.config import BROWSERKER_ACTIVE_CHECKS
from src.configuration import Configuration
from src.zap_gateway import Settings


class BrowserkerConfigurationFile:
    SETTINGS_TO_MASK_IN_LOG = ['UserName', 'Password', 'ClientCertificate', 'ClientCertificatePassword']

    def __init__(self, config: Configuration, file_path: str, secure_report_path: str):
        self._config = config
        self._file_path = file_path
        self._secure_report_path = secure_report_path

    def write(self) -> None:
        allowed_hosts = self.list_values(self._config.browserker_allowed_hosts)
        excluded_uris = self.list_values(self._config.exclude_urls, lambda url: url != self._config.auth_url)

        settings = [
            f'AllowedHosts = [{allowed_hosts}]',
            'DataPath = "/data/browserker"',
            f'ExcludedElements = [{self.list_values(self._config.browserker_excluded_elements)}]',
            f'ExcludedHosts = [{self.list_values(self._config.browserker_excluded_hosts)}]',
            f'ExcludedURIs = [{excluded_uris}]',
            f'IgnoredHosts = [{self.list_values(self._config.browserker_ignored_hosts)}]',
            f'MaxActions = {self._config.browserker_max_actions}',
            f'MaxDepth = {self._config.browserker_max_depth}',
            f'NumBrowsers = {self._config.browserker_number_of_browsers}',
            'PluginResourcePath = "/browserker/resources/"',
            'ReportCookiesPath = "/output/cookies.json"',
            f'ScanMode = "{self.scan_mode()}"',
            'ShowBrowser = false',
            'BrowserWidth = 1300',
            'BrowserHeight = 700',
            f'CustomHashAttributes = [{self.list_values(self._config.browserker_hash_attributes)}]',
            f'URL = "{self._config.target}"',
            f'SecureReport = "{self._secure_report_path}"',
        ]

        if self._config.browserker_active_check_workers:
            settings.append(f'ActiveCheckWorkers = {self._config.browserker_active_check_workers}')

        if self._config.browserker_passive_check_workers:
            settings.append(f'PassiveCheckWorkers = {self._config.browserker_passive_check_workers}')

        if self._config.fips_mode:
            settings.append('Proxy = "http://127.0.0.1:3128"')
            settings.append('FipsMode = true')

        elif self._config.zap_enabled and \
             (self._config.feature_flags is None or not self._config.feature_flags.get('bypassZap', False)):  # noqa: E127,E501

            settings.append(f'Proxy = "http://127.0.0.1:{self._config.zap_port}"')

        if self._config.browserker_secure_report_extra_info:
            settings.append('SecureReportExtraInfo = true')

        if self._config.browserker_log_request_error_report:
            settings.append('LogRequestErrorReport = true')

        if self._config.pkcs12_cert:
            b64cert = standard_b64encode(self._config.pkcs12_cert).decode()
            settings.append(f'ClientCertificate = "{b64cert}"')

        if self._config.pkcs12_cert and self._config.pkcs12_password:
            settings.append(f'ClientCertificatePassword = "{self._config.pkcs12_password}"')

        self.apply_timeouts(settings)

        settings.append(f'OnlyIncludeChecks = [{self.list_values(self.checks_to_run())}]')

        if self._config.browserker_max_response_size_mb != 0:
            settings.append(f'MaxResponseSizeMB = {self._config.browserker_max_response_size_mb}')

        if self._config.browserker_chrome_debug_log_dir:
            settings.append(f'ChromeDebugLogDir = "{self._config.browserker_chrome_debug_log_dir}"')

        if self._config.paths_to_scan_list:
            formatted_paths = [f'"{path}"' for path in self._config.paths_to_scan_list]
            settings.append(f'DirectPaths = [{",".join(formatted_paths)}]')

        if self._config.paths_to_scan_file_path:
            settings.append(f'DirectPathsFilePath = "{self._config.paths_to_scan_file_path}"')

        if self._config.browserker_page_loading_selector:
            settings.append(f'PageLoadingSelector = "{self._config.browserker_page_loading_selector}"')

        if self._config.browserker_page_ready_selector:
            settings.append(f'PageReadySelector = "{self._config.browserker_page_ready_selector}"')

        if self._config.browserker_crawl_report:
            settings.append(f'CrawlReportPath = "{Settings.WRK_DIR}gl-dast-debug-crawl-report.html"')

        if self._config.browserker_crawl_graph:
            settings.append(f'CrawlGraphImagePath = "{Settings.WRK_DIR}gl-dast-crawl-graph.svg"')

        disable_cache = 'false' if self._config.browserker_cache else 'true'
        settings.append(f'DisableCache = {disable_cache}')

        log_chromium_process_output = 'true' if self._config.browserker_log_chromium_output else 'false'
        settings.append(f'LogChromiumProcessOutput = {log_chromium_process_output}')

        if self._config.browserker_file_log_path:
            settings.append(f'FileLogPath = "{self._config.browserker_file_log_path}"')

        settings.extend(self.mapped_values('CustomHeaders', self.request_headers()))
        settings.extend(self.mapped_values('CustomCookies', self._config.browserker_cookies))
        settings.extend(self.mapped_values('FileLogLevels',
                                           self.sanitize_log_values('debug', self._config.browserker_file_log)))
        settings.extend(self.mapped_values('ConsoleLogLevels',
                                           self.sanitize_log_values('info', self._config.browserker_log)))
        settings.extend(self.mapped_values('DevToolsLogging', self._config.browserker_devtools_log, quote_key=True))
        settings.extend(self.feature_flag_values())
        settings.extend(self.database_details())
        settings.extend(self.authentication_details())

        self.log_settings(settings)

        with open(self._file_path, 'w') as file:
            for setting in settings:
                file.write(f'{setting}{linesep}')

    def apply_timeouts(self, settings: List[str]):
        if self._config.browserker_navigation_timeout:
            settings.append(f'NavigationTimeout = "{self._config.browserker_navigation_timeout}"')

        if self._config.browserker_action_timeout:
            settings.append(f'ActionTimeout = "{self._config.browserker_action_timeout}"')

        if self._config.browserker_stability_timeout:
            settings.append(f'StabilityTimeout = "{self._config.browserker_stability_timeout}"')

        if self._config.browserker_navigation_stability_timeout:
            settings.append(f'WaitAfterNavigation = "{self._config.browserker_navigation_stability_timeout}"')

        if self._config.browserker_action_stability_timeout:
            settings.append(f'WaitAfterAction = "{self._config.browserker_action_stability_timeout}"')

        if self._config.browserker_search_element_timeout:
            settings.append(f'SearchElementTimeout = "{self._config.browserker_search_element_timeout}"')

        if self._config.browserker_extract_element_timeout:
            settings.append(f'ExtractElementTimeout = "{self._config.browserker_extract_element_timeout}"')

        if self._config.browserker_element_timeout:
            settings.append(f'ElementTimeout = "{self._config.browserker_element_timeout}"')

        if self._config.browserker_active_scan_timeout:
            settings.append(f'ActiveScanTimeout = "{self._config.browserker_active_scan_timeout}"')

        if self._config.browserker_dom_ready_after_timeout:
            settings.append(f'DOMReadyAfterTimeout = "{self._config.browserker_dom_ready_after_timeout}"')

        if self._config.browserker_crawl_timeout:
            settings.append(f'CrawlTimeout = "{self._config.browserker_crawl_timeout}"')

        return

    def has_auth_details(self) -> bool:
        return self._config.auth_url is not None \
               and self._config.auth_username \
               and self._config.auth_password

    def list_values(self, values_list: List[str], accept: Callable[[str], bool] = lambda value: True) -> str:
        if not values_list:
            return ''

        values = [value for value in values_list if value and accept(value)]

        if not values:
            return ''

        comma_separated = '", "'.join(values)
        return f'"{comma_separated}"'

    def mapped_values(self, name: str, values: Dict[str, str], quote_key: bool = False) -> List[str]:
        if not values:
            return []

        settings = ['', f'[{name}]']

        for key, value in values.items():
            keyView = f'"{key}"' if quote_key else key
            settings.extend([f'{keyView} = "{value}"'])

        return settings

    def feature_flag_values(self) -> List[str]:
        if not self._config.feature_flags:
            return []

        settings = ['', '[FeatureFlags]']

        for name, value in self._config.feature_flags.items():
            normalized_name = name[0].capitalize() + name[1:]
            settings.extend([f'{normalized_name} = {"true" if value else "false"}'])

        return settings

    def database_details(self) -> List[str]:
        if not self._config.browserker_db_memory_table_size:
            return []

        return [
            '',
            '[DatabaseDetails]',
            f'MemoryTableSize = {self._config.browserker_db_memory_table_size}',
        ]

    def authentication_details(self) -> List[str]:
        if not self.has_auth_details():
            return []

        username = self._config.auth_username.encode('unicode-escape').decode()
        password = self._config.auth_password.encode('unicode-escape').decode()

        settings = [
            '',
            '[AuthDetails]',
            f'LoginURL = "{self._config.auth_url}"',
            f'UserName = "{username}"',
            f'Password = "{password}"',
        ]

        if self._config.auth_type:
            settings.append(f'AuthType = "{self._config.auth_type}"')
        if self._config.auth_username_field:
            settings.append(f'UserNameField = "{self._config.auth_username_field}"')
        if self._config.auth_first_submit_field:
            settings.append(f'UserNameSubmitField = "{self._config.auth_first_submit_field}"')
        if self._config.auth_password_field:
            settings.append(f'PasswordField = "{self._config.auth_password_field}"')
        if self._config.auth_submit_field:
            settings.append(f'SubmitButtonField = "{self._config.auth_submit_field}"')
        if self._config.auth_verification_url:
            settings.append(f'VerificationURL = "{self._config.auth_verification_url}"')
        if self._config.auth_verification_selector:
            settings.append(f'VerificationSelector = "{self._config.auth_verification_selector}"')
        if self._config.auth_verification_login_form:
            settings.append('VerificationLoginForm = true')
        if self._config.auth_path_to_login_form:
            settings.append(f'PathToLoginForm = [{self.list_values(self._config.auth_path_to_login_form)}]')
        if self._config.auth_cookies:
            settings.append(f'Cookies = [{self.list_values(self._config.auth_cookies)}]')
        if self._config.auth_report:
            settings.append(f'ReportPath = "{Settings.WRK_DIR}gl-dast-debug-auth-report.html"')
        if self._config.browserker_always_relogin:
            settings.append('AlwaysRelogin = true')
        if self._config.auth_disable_clear_fields:
            settings.append('ClearFields = false')

        if self._config.auth_username_field and \
                self._config.auth_password_field and \
                not self._config.auth_submit_field:
            settings.append('SubmitButtonField = "css:[type=submit], button"')

        return settings

    def sanitize_log_values(self, default: str, moduleLevels: Dict[str, str]) -> Dict[str, str]:
        sanitized = {'LogLevel': default}

        for module, level in (moduleLevels or {}).items():
            if module.upper() == 'LOGLEVEL':
                sanitized['LogLevel'] = level.lower()
            else:
                sanitized[module.upper()] = level.lower()

        return sanitized

    def request_headers(self) -> Dict[str, str]:
        user_headers = self._config.request_headers or {}
        dast_headers = {'Via-Scanner': 'Browserker'} if self._config.advertise_scan else {}
        return {**user_headers, **dast_headers}

    def log_settings(self, settings: List[str]) -> None:
        for setting in settings:
            setting_name = setting.split('=')[0].strip()

            if setting_name in self.SETTINGS_TO_MASK_IN_LOG or setting_name in self.request_headers().keys():
                setting = f'{setting_name} = ********'

            logging.info(f'Adding Browserker setting {setting}')

    def checks_to_run(self) -> List[str]:
        if self._config.browserker_include_only_rules:
            return self._config.browserker_include_only_rules

        passive = [
            '16.1', '16.2', '16.3', '16.4', '16.5', '16.6', '16.7', '16.8', '16.9', '16.10',
            '200.1', '209.1', '209.2', '287.1', '287.2',
            '319.1', '352.1', '359.1', '359.2',
            '548.1', '598.1', '598.2', '598.3',
            '601.1', '614.1', '693.1',
            ]+self._check_798_ids()+[
            '829.1', '829.2',
            '1004.1',
        ]

        return passive + BROWSERKER_ACTIVE_CHECKS.enabled_checks(self._config.feature_flags)

    def scan_mode(self) -> str:
        return 'active' if self._config.full_scan else 'passive'

    def _check_798_ids(self) -> List[str]:
        removed_rules = ['798.45', '798.51', '798.71', '798.73', '798.76', '798.79', '798.85']
        return [f'798.{point_number}' for point_number in range(1, 129) if f'798.{point_number}' not in removed_rules]

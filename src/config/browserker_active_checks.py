from dataclasses import dataclass
from typing import Dict, List


@dataclass
class BrowserkerActiveCheck:
    check_id: str = None
    replaced_zap_check: str = None
    alpha: bool = False  # Alpha checks are promoted to non-alpha (release) when we're confident they work.
    callback_attacks: List[str] = None


class BrowserkerActiveChecks:

    def __init__(self, checks: List[BrowserkerActiveCheck]):
        self.checks = checks

    def enabled_checks(self, feature_flags: Dict[str, bool]) -> List[str]:
        if not feature_flags:
            return []

        check_ids = []

        if feature_flags.get('enableBrowserBasedAlphaAttacks', False):
            check_ids += {check.check_id for check in self.checks}

        if feature_flags.get('enableBrowserBasedAttacks', False):
            check_ids += {check.check_id for check in self.checks if not check.alpha}

        if feature_flags.get('enableBas', False):
            check_ids += [attack_id
                          for check in self.checks if check.callback_attacks
                          for attack_id in check.callback_attacks]

        return self._deduplicate(check_ids)

    def replaced_zap_rules(self, feature_flags: Dict[str, bool]) -> List[str]:
        check_id_to_replaced_ids = {check.check_id: check.replaced_zap_check for check in self.checks}
        replaced_zap_check_ids = [check_id_to_replaced_ids[check_id] for check_id in self.enabled_checks(feature_flags)
                                  if check_id in check_id_to_replaced_ids]

        return self._deduplicate(replaced_zap_check_ids)

    def _deduplicate(self, check_ids: List[str]) -> List[str]:
        sorted_checks = sorted(check_ids)
        checks = set()

        for check_id in sorted_checks:
            if check_id in checks:
                continue

            id_parts = check_id.split('.')
            if len(id_parts) > 2 and f'{id_parts[0]}.{id_parts[1]}' in checks:
                continue

            checks.add(check_id)

        return sorted(checks)


BROWSERKER_ACTIVE_CHECKS = BrowserkerActiveChecks([
    BrowserkerActiveCheck('22.1', replaced_zap_check='6', alpha=False),
    BrowserkerActiveCheck('78.1', replaced_zap_check='90020', alpha=True),
    BrowserkerActiveCheck('94.1', replaced_zap_check='90019', alpha=True),
    BrowserkerActiveCheck('94.2', replaced_zap_check='90019', alpha=True),
    BrowserkerActiveCheck('94.3', replaced_zap_check='90019', alpha=True),
    BrowserkerActiveCheck('94.4', replaced_zap_check='90019', alpha=True, callback_attacks=['94.4.2']),
    BrowserkerActiveCheck('113.1', replaced_zap_check='40003', alpha=True),
    BrowserkerActiveCheck('611.1', replaced_zap_check='90023', alpha=True),
    BrowserkerActiveCheck('943.1', replaced_zap_check='40033', alpha=True),
])

import logging
import socket
import time
from contextlib import closing

from .models.errors import ProxyFailedToStartError


class Proxy:
    """Methods related to proxy service
    """

    def wait_for_proxy(
            self,
            host: str = '127.0.0.1',
            port: int = 3128,
            max_tries: int = 10,
            wait_between_checks: float = 1) -> None:
        """Wait for proxy port to become available.

        This method waits for the proxies TCP port to become available by connecting to it.

        Args:
            host (str, optional): Proxy host. Defaults to '127.0.0.1'.
            port (int, optional): Proxy port. Defaults to 3128.
            max_tries (int, optional): Maximum times to try connecting to proxy port. Defaults to 10.
            wait_between_checks (float, optional): How long to wait between checks in seconds. Defaults to 1.

        Raises:
            ProxyFailedToStartError: Raised if the proxy fails to start in allotted time window
        """
        logging.info('Waiting for proxy service to become available...')
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            sock.setblocking(False)
            for cnt in range(max_tries):
                if sock.connect_ex((host, port)) == 0:
                    logging.info('Proxy service is available')
                    return

                logging.debug(f'Proxy service not yet available, try {cnt} of {max_tries}.')
                time.sleep(wait_between_checks)

        raise ProxyFailedToStartError(
            'Proxy service failed to start, please contact support for assistance.')

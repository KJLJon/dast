from bs4 import BeautifulSoup


class HTMLContent:
    NON_TEXT_HTML_TAGS = ['[document]',
                          'html',
                          'head',
                          'style',
                          'script',
                          'noscript']

    def __init__(self, html):
        self.html = BeautifulSoup(html, 'html5lib')

    def text(self):
        text_items = self.html.find_all(text=True)
        include_items = [item.strip() for item in text_items if item.parent.name not in self.NON_TEXT_HTML_TAGS]
        non_empty_items = [text for text in include_items if text]

        return ' '.join(non_empty_items)

    def list_of_text_in_tag(self, tag_name):
        tags = self.html.find_all(tag_name)
        include_items = [item.get_text().strip() for item in tags]
        return [text for text in include_items if text]

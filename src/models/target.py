from re import escape
from urllib.parse import urlparse

from .errors import InvalidTargetError


class Target(str):

    def __bool__(self) -> bool:
        if self == 'None' or self == '':
            return False

        return True

    @property
    def regex(self) -> str:
        uri = urlparse(self)

        return escape(f'{uri.scheme}://{uri.netloc}')

    def is_host_url(self) -> bool:
        return self.count('/') <= 2

    def reset_to_host(self) -> 'Target':
        if self.is_host_url():
            return self

        return self.__class__(self[0:self.index('/', 8)])

    def hostname(self) -> str:
        host = urlparse(self).hostname

        if host is None:
            raise InvalidTargetError('Target must have a valid hostname')

        return host

    def build_url(self, path: str) -> str:
        return f"{self}/{path.strip().strip('/')}"

    def external_js(self) -> str:
        return fr'^(?!{self.regex}).*\.js'

    def external_css(self) -> str:
        return fr'^(?!{self.regex}).*\.css'

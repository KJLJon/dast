# Debugging DAST and ZAP locally

## Overview

In order to troubleshoot a complex problem with DAST/ZAP, it may be necessary to debug the ZAP server that runs inside the Docker container.
These instructions will show you how to compile ZAP, incorporate it into the DAST Docker image, and attach a remote debugger to it during a running DAST scan.

## How to build ZAP and incorporate it in a DAST Docker image

1. Install ZAP
   - Install Java 11 (suggestion: use asdf)
     1. `asdf plugin-add java`
     1. `asdf install java adoptopenjdk-11.0.16+101`
     1. `asdf local java adoptopenjdk-11.0.16+101` (might need to make this `global`)
   - Install ZAP and ZAP add-ons
     1. `invoke zap.project.install`
1. Change ZAP to your liking (this includes ZAP core, or any of the add-ons)
   - Load the zap folder in a Java IDE. Recommendation is IntelliJ, however, the ZAP team use Eclipse.
   - e.g. Add logging statements, remove/add code
   - You won't easily be able to do anything that requires a new library
1. Build the ZAP distribution
   - `invoke zap.project.build`
   - Copy the resulting `.zip` file into the root of the DAST project directory
1. Change the DAST Dockerfile to include your locally built version of ZAP in your DAST project directory
   - Apply the following changes, changing the `ZAP_D-[date]` to the date you built ZAP (likely your today)

   ```sh
     COPY ZAP_D-2022-10-03.zip /browserker/ZAP_D-2022-10-03.zip

     # Install ZAP
     RUN usermod --uid $BROWSERKER_UID gitlab && \
     useradd --uid $ZAP_UID --create-home --shell /usr/bin/bash zap && \
     unzip ZAP_D-2022-10-03.zip && \
     mv ZAP_D-2022-10-03 /zap && \
     rm ZAP_D-2022-10-03.zip && \
     sed -i "s/exec java /exec java -Dlog4j2.formatMsgNoLookups=true -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 /g" /zap/zap.sh && \
     chown -R zap:zap /zap
   ```

   - Remove the entire previous [# Install ZAP](https://gitlab.com/gitlab-org/security-products/dast/-/blob/49fd62136c8f016fed1d4bbd99bacadf27bf47b5/Dockerfile#L50) section
1. Build the DAST Docker image. Done!

## How to debug ZAP in a running DAST scan

1. Build and use ZAP, as described above
1. In your Java IDE, create a Remote Debugging Connection to Port 5005
1. Place breakpoints in the Java source
1. Run DAST, exposing the port 5005
1. Start the remote debugging connection. The breakpoints will be triggered when reached in the scan.

#!/usr/bin/env bash
# Testing framework: https://github.com/pgrange/bash_unit

DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)")"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

test_fips_enabled() {

  # Are we running in FIPS mode?
  if [[ "$FIPS_MODE" == "1" ]]; then
    # Make sure fips is enabled
    if [[ -e "/proc/sys/crypto/fips_enabled" && "$(cat /proc/sys/crypto/fips_enabled)" == "1" ]]; then
      echo "FIPS is enabled in the OS"
    else
      echo "FIPS is not enabled in the OS"
      assert_equals 0 1 "FIPS is not enabled in the OS"
    fi
  fi
}

test_fips_proxy_started() {

  skip_if_not_fips

  local container_id
  local runtime
  local endtime
  local rc

  # Start the container up and keep it running for a while
  container_id=$(docker run --rm --detach "$BUILT_IMAGE" sh -c "sleep 1000")

  # Check for squid process
  # Initialization time varies, so we wait for up to N minutes

  runtime="3 minutes"
  endtime=$(date -ud "$runtime" +%s)
  assert_ret=0
  while [[ $(date -u +%s) -le $endtime ]]
  do
    # Check for squid proces
    docker exec "$container_id" ps ax | grep "/var/squid/sbin/squid" >/dev/null 2>&1
    assert_ret=$?

    if [[ $assert_ret -eq 0 ]]; then
      break
    fi

    sleep 0.2
  done
  if [[ $assert_ret -ne 0 ]]; then
    docker stop "$container_id"
    assert_equals "0" "$assert_ret" "Expected to find /var/squid/sbin/squid in the process list"
  fi

  # Check for security_file_certgen process
  # Initialization time varies, so we wait for up to N minutes

  runtime="3 minutes"
  endtime=$(date -ud "$runtime" +%s)
  assert_ret=0
  while [[ $(date -u +%s) -le $endtime ]]
  do
    # Check for security_file_certgen process
    docker exec "$container_id" ps ax | grep "security_file_certgen" >/dev/null 2>&1
    assert_ret=$?

    if [[ $assert_ret -eq 0 ]]; then
      break
    fi

    sleep 0.2
  done
  if [[ $assert_ret -ne 0 ]]; then
    docker stop "$container_id"
    assert_equals "0" "$assert_ret" "Expected to find security_file_certgen in the process list"
  fi

  docker exec "$container_id" curl --silent --proxy http://localhost:3128 https://gitlab.com  >> output/test_fips_proxy_started-cmds.log 2>&1
  rc=$?
  docker stop "$container_id"
  assert_equals "0" "$rc" "Expected to fetch https://gitlab.com via squid without errors"
}

test_fips_py_tls_config() {

  # This test must run in both FIPS and non-FIPS mode
  # to provide proper assurances.

  NON_FIPS_STATUS_CODE=${NON_FIPS_STATUS_CODE:-1}
  NON_FIPS_CIPHER=DHE-RSA-CHACHA20-POLY1305
  FIPS_CIPHER=DHE-RSA-AES128-SHA256
  COMPOSE_CMD="docker-compose \
    --file $DIR/fixtures/fipscryptopolicy/docker-compose-py.yml \
    --file $DIR/fixtures/fipscryptopolicy/docker-compose-tls.yml \
    up \
    --build \
    --remove-orphans \
    --abort-on-container-exit \
    --exit-code-from dast-fips \
    ";

  # Are we running in FIPS mode?
  if [[ "$FIPS_MODE" -ne "1" ]]; then
    # Running this test when not in FIPS mode makes
    # sure our test isn't broken in some way.

    # When running w/o fips, change the expected exit code
    NON_FIPS_STATUS_CODE=0
  fi

  FIPS_CIPHER_CMD="TLS_CIPHER=${FIPS_CIPHER} ${COMPOSE_CMD}"
  echo "> $FIPS_CIPHER_CMD" >>output/test_fips_py_tls_config-cmds.log 2>&1
  eval "$FIPS_CIPHER_CMD" >>output/test_fips_py_tls_config-cmds.log 2>&1
  assert_equals 0 $? "Did not get expected result connecting with FIPS-compatible cipher:"
  grep -a ":SSL routines:tls_post_process_client_hello:no shared cipher:" output/test_fips_py_tls_config-cmds.log >/dev/null
  assert_equals 1 $? "Should not find 'no shared cipher' in log file"

  NON_FIPS_CIPHER_CMD="TLS_CIPHER=${NON_FIPS_CIPHER} ${COMPOSE_CMD}"
  echo "> $NON_FIPS_CIPHER_CMD" >>output/test_fips_py_tls_config-cmds.log 2>&1
  eval "$NON_FIPS_CIPHER_CMD" >>output/test_fips_py_tls_config-cmds.log 2>&1
  assert_equals "$NON_FIPS_STATUS_CODE" $? "Did not get expected result connecting with non-FIPS-compatible cipher:"

  if [[ "$FIPS_MODE" == "1" ]]; then
    grep -a ":SSL routines:tls_post_process_client_hello:no shared cipher:" output/test_fips_py_tls_config-cmds.log > /dev/null
    assert_equals 0 $? "Should find 'no shared cipher' in log file"
  else
    grep -a ":SSL routines:tls_post_process_client_hello:no shared cipher:" output/test_fips_py_tls_config-cmds.log > /dev/null
    assert_equals 1 $? "Should not find 'no shared cipher' in log file"
  fi
}

test_fips_verify_squid_used_by_dast_chromium() {

  # Using a server that doesn't have a FIPS compliant
  # cipher suite, we can verify that DAST is using the
  # Squid proxy or other means to be FIPS compliant.

  # NOTE: This test skips the target health check to focus
  #       on Browserkers use of Chromium.

  # This test must run in both FIPS and non-FIPS mode
  # to provide proper assurances.

  NON_FIPS_STATUS_CODE=${NON_FIPS_STATUS_CODE:-0}
  NON_FIPS_CIPHER=DHE-RSA-CHACHA20-POLY1305
  FIPS_CIPHER=DHE-RSA-AES128-SHA256
  COMPOSE_CMD="docker-compose \
    --file $DIR/fixtures/fipscryptopolicy/docker-compose-dast.yml \
    --file $DIR/fixtures/fipscryptopolicy/docker-compose-tls.yml \
    up \
    --build \
    --remove-orphans \
    --abort-on-container-exit \
    --exit-code-from dast-fips \
    ";

  # Are we running in FIPS mode?
  if [[ "$FIPS_MODE" -ne "1" ]]; then
    # Running this test when not in FIPS mode makes
    # sure our test isn't broken in some way.

    # When running w/o fips, change the expected exit code
    NON_FIPS_STATUS_CODE=0
  fi

  FIPS_OUTPUT=output/test_fips_verify_squid_used_by_dast-cmds-fips.log
  NON_FIPS_OUTPUT=output/test_fips_verify_squid_used_by_dast-cmds-non_fips.log

  FIPS_CIPHER_CMD="DAST_SKIP_TARGET_CHECK=true TLS_CIPHER=${FIPS_CIPHER} ${COMPOSE_CMD}"
  echo "> $FIPS_CIPHER_CMD" >>$FIPS_OUTPUT 2>&1
  eval "$FIPS_CIPHER_CMD" >>$FIPS_OUTPUT 2>&1
  assert_equals 0 $? "Did not get expected result connecting with FIPS-compatible cipher:"
  grep -a ":SSL routines:tls_post_process_client_hello:no shared cipher:" $FIPS_OUTPUT >/dev/null
  assert_equals 1 $? "Should not find 'no shared cipher' in log file $FIPS_OUTPUT"
  grep -a "FILE:resource.txt" $FIPS_OUTPUT >/dev/null
  assert_equals 0 $? "Should find 'FILE:resource.txt' in log file $FIPS_OUTPUT"

  NON_FIPS_CIPHER_CMD="DAST_SKIP_TARGET_CHECK=true TLS_CIPHER=${NON_FIPS_CIPHER} ${COMPOSE_CMD}"
  echo "> $NON_FIPS_CIPHER_CMD" >>$NON_FIPS_OUTPUT 2>&1
  eval "$NON_FIPS_CIPHER_CMD" >>$NON_FIPS_OUTPUT 2>&1
  assert_equals "$NON_FIPS_STATUS_CODE" "$?" "Did not get expected result connecting with non-FIPS-compatible cipher:"

  if [[ "$FIPS_MODE" == "1" ]]; then
    grep -a ":SSL routines:tls_post_process_client_hello:no shared cipher:" $NON_FIPS_OUTPUT >/dev/null
    assert_equals 0 $? "Should find 'no shared cipher' in log file $NON_FIPS_OUTPUT"
    grep -a "FILE:resource.txt" $NON_FIPS_OUTPUT >/dev/null
    assert_equals 1 $? "Should not find 'FILE:resource.txt' in log file $NON_FIPS_OUTPUT"
  else
    grep -a ":SSL routines:tls_post_process_client_hello:no shared cipher:" $NON_FIPS_OUTPUT > /dev/null
    assert_equals 1 $? "Should not find 'no shared cipher' in log file"
    grep -a "FILE:resource.txt" $NON_FIPS_OUTPUT >/dev/null
    assert_equals 0 $? "Should find 'FILE:resource.txt' in log file $NON_FIPS_OUTPUT"
  fi
}

#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}
MAX_SCAN_DURATION_SECONDS=${MAX_SCAN_DURATION_SECONDS:-66}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force opt-param-test >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_baseline_scan_using_t() {
  skip_if_fips "Expectations are ZAP-specific"

  START_TIME=$(date +%s)
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    -v "${PWD}/fixtures/scripts":/home/zap/custom-scripts \
    --network test \
    --env DAST_MASK_HTTP_HEADERS=Cache-Control,Pragma \
    --env DAST_SCRIPT_DIRS=/home/zap/custom-scripts \
    --env DAST_ADVERTISE_SCAN=true \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_baseline_scan_using_t.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"
  SCAN_DURATION=$(($(date +%s) - START_TIME))

  jq . < gl-dast-report.json > output/report_test_baseline_scan_using_t.json

  assert_output test_baseline_scan_using_t output/report_test_baseline_scan_using_t
  assert "test $MAX_SCAN_DURATION_SECONDS -gt $SCAN_DURATION" "Test took $SCAN_DURATION seconds, which is longer than the allowed $MAX_SCAN_DURATION_SECONDS seconds"

  grep -q 'The following URLs were scanned:' output/test_baseline_scan_using_t.log && \
  grep -q 'GET http://nginx/' output/test_baseline_scan_using_t.log && \
  grep -q 'POST http://nginx/myform' output/test_baseline_scan_using_t.log
  assert_equals "0" "$?" "Logged output differs from expectation"

  ./verify-dast-schema.py output/report_test_baseline_scan_using_t.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"

  grep -q '{"name": "Cache-Control", "value": "\*\*\*\*\*\*\*\*"}' gl-dast-report.json && \
  grep -q '{"name": "Pragma", "value": "\*\*\*\*\*\*\*\*"}' gl-dast-report.json
  assert_equals "0" "$?" "Headers were not masked in report output"

  grep -q '{"name": "x-initiated-by", "value": "GitLab DAST"}' gl-dast-report.json
  assert_equals "0" "$?" "Initiated by script is not injecting custom header into requests"

  grep -q "Via" gl-dast-report.json &&
  grep -q "GitLab DAST/ZAP v" gl-dast-report.json &&
  assert_equals "0" "$?" "Report does not include DAST Via request headers"
}

test_baseline_scan_using_https_with_target_env_variable() {
  skip_if_fips "Expectations are ZAP-specific"

  ./docker_run_dast \
    --name 'opt-param-test' \
    -v "${PWD}":/output --network test \
    -e DAST_WEBSITE=https://nginx "${BUILT_IMAGE}" /analyze -d -j -z "-config database.request.bodysize=12345678" \
    >output/test_baseline_scan_using_https_with_target_env_variable.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_baseline_scan_using_https_with_target_env_variable.json

  assert_output test_baseline_scan_using_https_with_target_env_variable output/report_test_baseline_scan_using_https_with_target_env_variable

  grep -q "Running ZAP with parameters.*12345678" output/test_baseline_scan_using_https_with_target_env_variable.log
  assert_equals "0" "$?" "Optional parameter was not passed to ZAProxy"

  ./verify-dast-schema.py output/report_test_baseline_scan_using_https_with_target_env_variable.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"

  grep -q "Via" gl-dast-report.json
  assert_equals "1" "$?" "Report includes DAST Via request header when it should not"
}

test_baseline_scan_does_not_aggregate_noisy_vulnerabilities() {
  skip_if_fips "Expectations are ZAP-specific"

  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_AGGREGATE_VULNERABILITIES=False \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_baseline_scan_does_not_aggregate_noisy_vulnerabilities.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_baseline_scan_does_not_aggregate_noisy_vulnerabilities.json

  ./verify-dast-schema.py output/report_test_baseline_scan_does_not_aggregate_noisy_vulnerabilities.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"

  assert_output test_baseline_scan_does_not_aggregate_noisy_vulnerabilities output/report_test_baseline_scan_does_not_aggregate_noisy_vulnerabilities
}

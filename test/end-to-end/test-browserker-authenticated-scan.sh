#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies

  # setup test docker network and start the basic site nginx server
  run_basic_site

  # start Rails Goat
  docker run --rm \
    --name railsgoat \
    --network test \
    -d \
    registry.gitlab.com/gitlab-org/security-products/dast/railsgoat-authentication-end-to-end-test >/dev/null

  true
}

teardown_suite() {
  docker rm -f railsgoat nginx >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_nginx_browserker_authenticated_scan_with_env_vars() {
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    -e DAST_AUTH_TYPE=basic-digest \
    -e DAST_AUTH_URL=http://nginx/admin.html \
    -e DAST_PASSWORD=xkcd.com/327 \
    -e DAST_USERNAME=bobbytables \
    -e DAST_BROWSER_SCAN=true \
    -e DAST_BROWSER_MAX_ACTIONS=10 \
    -e DAST_BROWSER_MAX_DEPTH=2 \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx \
    >output/test_nginx_browserker_authenticated_scan_with_env_vars.log 2>&1

  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q "xkcd\.com/327" output/test_nginx_browserker_authenticated_scan_with_env_vars.log
  assert_equals "1" "$?" "Password present in logs"

  grep -q "browser sent authentication challenge to access the resource, but basic-digest authentication not configured" output/test_nginx_browserker_authenticated_scan_with_env_vars.log
  assert_equals "1" "$?" "Expected basic authentication challenge to be sent"

  grep -q "Failure while running Browserker" output/test_nginx_browserker_authenticated_scan_with_env_vars.log
  assert_equals "1" "$?" "Browserker failed with unexpected error"

  grep -q "login attempt succeeded" output/test_nginx_browserker_authenticated_scan_with_env_vars.log
  assert_equals "0" "$?" "Browserker failed to authenticate the user"

  jq . < gl-dast-report.json > output/report_test_nginx_browserker_authenticated_scan_with_env_vars.json

  jq '.scan.scanned_resources[].url' output/report_test_nginx_browserker_authenticated_scan_with_env_vars.json | grep -q "http://nginx/admin"
  assert_equals "0" "$?" "Expected URL accessible after authentication to be included in scanned resources"
}

test_nginx_browserker_authentication_failure_with_env_vars() {
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    -e DAST_AUTH_TYPE=basic-digest \
    -e DAST_AUTH_URL=http://nginx/admin \
    -e DAST_USERNAME=bobbytables \
    -e DAST_PASSWORD=wrongpassword \
    -e DAST_BROWSER_SCAN=true \
    -e DAST_BROWSER_MAX_ACTIONS=10 \
    -e DAST_BROWSER_MAX_DEPTH=2 \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx \
    >output/test_nginx_browserker_authentication_failure_with_env_vars.log 2>&1

  assert_equals "1" "$?" "Expected to exit from failed authentication"

  grep -q "Failure while running Browserker" output/test_nginx_browserker_authentication_failure_with_env_vars.log && \
  grep -q "authentication failed: failed to load login page due to invalid credentials" output/test_nginx_browserker_authentication_failure_with_env_vars.log
  assert_equals "0" "$?" "Error message not printed as expected"
}

test_nginx_browserker_authenticated_scan_with_flags() {
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx \
    --auth-type basic-digest \
    --auth-url http://nginx/admin.html \
    --auth-username "bobbytables" \
    --auth-password "xkcd.com/327" \
    --browser-scan \
    --browser-max-actions 10 \
    --browser-max-depth 2 \
    >output/test_nginx_browserker_authenticated_scan_with_flags.log 2>&1

  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q "xkcd\.com/327" output/test_nginx_browserker_authenticated_scan_with_flags.log
  assert_equals "1" "$?" "Password present in logs"

  grep -q "browser sent authentication challenge to access the resource, but basic-digest authentication not configured" output/test_nginx_browserker_authenticated_scan_with_flags.log
  assert_equals "1" "$?" "Expected basic authentication challenge to be sent"

  grep -q "Failure while running Browserker" output/test_nginx_browserker_authenticated_scan_with_flags.log
  assert_equals "1" "$?" "Browserker failed with unexpected error"

  jq . < gl-dast-report.json > output/report_test_nginx_browserker_authenticated_scan_with_flags.json

  jq '.scan.scanned_resources[].url' output/report_test_nginx_browserker_authenticated_scan_with_flags.json | grep -q "http://nginx/admin"
  assert_equals "0" "$?" "Expected URL accessible after authentication to be included in scanned resources"
}

test_nginx_browserker_authentication_failure_with_flags() {
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx \
    --auth-type basic-digest \
    --auth-url http://nginx/admin \
    --auth-username "bobbytables" \
    --auth-password "wrongpassword" \
    --browser-scan \
    --browser-max-actions 10 \
    --browser-max-depth 2 \
    >output/test_nginx_browserker_authentication_failure_with_flags.log 2>&1

  assert_equals "1" "$?" "Expected to exit from failed authentication"

  grep -q "Failure while running Browserker" output/test_nginx_browserker_authentication_failure_with_flags.log && \
  grep -q "authentication failed: failed to load login page due to invalid credentials" output/test_nginx_browserker_authentication_failure_with_flags.log
  assert_equals "0" "$?" "Error message not printed as expected"
}

test_railsgoat_browserker_authenticated_scan() {
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t http://railsgoat:3001 \
    --browser-scan \
    --browser-max-actions 10 \
    --browser-max-depth 2 \
    --auth-url http://railsgoat:3001/login? \
    --auth-verification-url http://railsgoat:3001/dashboard/home \
    --auth-username "ken@metacorp.com" \
    --auth-password "citrusblend" \
    --auth-exclude-urls 'http://railsgoat:3001/logout' \
    --browser-auth-verification-selector "id:mainnav" \
    --browser-auth-verification-login-form \
    >output/test_railsgoat_browserker_authenticated_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_railsgoat_browserker_authenticated_scan.json

  grep -q "Failure while running Browserker" output/test_railsgoat_browserker_authenticated_scan.log && \
  assert_equals "1" "$?" "Browserker failed with unexpected error"

  jq '.scan.scanned_resources[].url' output/report_test_railsgoat_browserker_authenticated_scan.json | grep -q "http://railsgoat:3001/users/6"
  assert_equals "0" "$?" "Expected URL accessible after authentication to be included in scanned resources"
}

test_railsgoat_browserker_authentication_failure() {
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t http://railsgoat:3001 \
    --browser-scan \
    --browser-auth-report \
    --auth-url http://railsgoat:3001/login? \
    --auth-verification-url http://railsgoat:3001/dashboard/home \
    --auth-username "ken@metacorp.com" \
    --auth-password "wrongpassword" \
    --auth-username-field "[id=email]" \
    --auth-password-field "[id=password]" \
    --auth-submit-field "[name=commit]" \
    --auth-exclude-urls 'http://railsgoat:3001/logout' \
    >output/test_railsgoat_browserker_authentication_failure.log 2>&1

  assert_equals "1" "$?" "Expected to exit from failed authentication"

  grep -q "Failure while running Browserker" output/test_railsgoat_browserker_authentication_failure.log && \
  grep -q "requirement is unsatisfied, browser is not at URL" output/test_railsgoat_browserker_authentication_failure.log && \
  grep -q "failed to authenticate user" output/test_railsgoat_browserker_authentication_failure.log
  assert_equals "0" "$?" "Error message not printed as expected"

  grep -q "Login page" gl-dast-debug-auth-report.html
  assert_equals "0" "$?" "Login page was not found in report"

  grep -q "GET http://railsgoat:3001/login?" gl-dast-debug-auth-report.html
  assert_equals "0" "$?" "Load login page request was not found in report"

  grep -q "&lt;!DOCTYPE html&gt;&lt;html&gt;&lt;head&gt;" gl-dast-debug-auth-report.html
  assert_equals "0" "$?" "DOM was not contained in report"

  grep -q "POST http://railsgoat:3001/sessions" gl-dast-debug-auth-report.html
  assert_equals "0" "$?" "Login submit request not found in report"

  grep -q "Empty request" gl-dast-debug-auth-report.html
  assert_equals "1" "$?" "Report contained an empty request"

  grep -q "Empty response" gl-dast-debug-auth-report.html
  assert_equals "1" "$?" "Report contained an empty response"
}

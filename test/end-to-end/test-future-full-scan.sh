#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast-future}

setup_suite() {
  setup_test_dependencies
  run_ajax_spider_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_future_full_scan() {
  skip_if_fips "Expectations are ZAP-specific"

  ./docker_run_dast --rm \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_EXCLUDE_RULES=10104,10027,20012,10109 \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" \
    /analyze -d -j -t http://nginx/food.html \
    >output/test_future_full_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_future_full_scan.json

  assert_output test_future_full_scan output/report_test_future_full_scan

  ./verify-dast-schema.py output/report_test_future_full_scan.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}

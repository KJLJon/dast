#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  docker network create test >/dev/null

  docker run \
    --name check-22-1 \
    -v "${PWD}/fixtures/check-22-1/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/fixtures/check-22-1/index.html:/usr/share/nginx/html/index.html" \
    --network test -d openresty/openresty:1.19.9.1-9-buster-fat >/dev/null 2>&1
}

teardown_suite() {
  docker rm --force check-22-1  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_active_check_22_1() {
  # Only include the 22.1 browserker check. Exclude ZAP checks that the scan detects, ensuring that ZAP check 6 is included.
  # This allows the test to verify that the inclusion of browserker check 22.1 will exclude ZAP check 6.
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_EXCLUDE_RULES=40012,90020,10095,10106 \
    --env DAST_BROWSER_INCLUDE_ONLY_RULES="22.1" \
    --env DAST_FF_ENABLE_BROWSER_BASED_ATTACKS="true" \
    --env DAST_BROWSER_LOG="brows:debug,chrom:trace" \
    --env DAST_BROWSER_DEVTOOLS_LOG="Default:suppress; Fetch:messageAndBody,truncate:2000; Network:messageAndBody,truncate:2000" \
    --env DAST_BROWSER_CRAWL_GRAPH="True" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://check-22-1 >output/test_browserker_active_check_22_1.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . <gl-dast-report.json >output/report_test_browserker_active_check_22_1.json
  rm -rf ./browserker_data
  mv ./findings.json output/report_test_browserker_active_check_22_1_findings.json
  mv ./gl-dast-crawl-graph.svg output/report_test_browserker_active_check_22_1-crawl-graph.svg

  assert_output test_browserker_active_check_22_1 output/report_test_browserker_active_check_22_1

  ./verify-dast-schema.py output/report_test_browserker_active_check_22_1.json
}

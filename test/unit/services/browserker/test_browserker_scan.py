from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, patch

from src.models.target import Target
from src.services import BrowserkerScan
from test.unit.mock_config import ToConfig


class TestBrowserkerScan(TestCase):

    def setUp(self) -> None:
        self.config = ToConfig(browserker_allowed_hosts=[])
        self.target = Target('http://example.com/path/1')

    def test_run_starts_browserker_process(self):
        with patch.multiple('src.services.browserker_scan',
                            json=DEFAULT,
                            open=DEFAULT,
                            os=DEFAULT,
                            System=DEFAULT,
                            BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            process.stdout.readline.return_value = bytes('', 'utf-8')
            mock_dependencies['System'].return_value = system
            system.run_piped_output.return_value = process
            BrowserkerScan(self.config, self.target, MagicMock()).run()

        mock_dependencies['BrowserkerConfigurationFile'].return_value.write.assert_called_once()
        system.run_piped_output.assert_called_once()

        self.assertIn('/browserker/analyzer', system.run_piped_output.mock_calls[0][1][0])
        self.assertIn('run', system.run_piped_output.mock_calls[0][1][0])

    def test_run_sets_global_exclude_urls(self):
        self.config.browserker_allowed_hosts = ['foo.com', 'bar.org', 'example.com']
        with patch.multiple('src.services.browserker_scan',
                            json=DEFAULT,
                            open=DEFAULT,
                            os=DEFAULT,
                            System=DEFAULT,
                            BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            process.stdout.readline.return_value = bytes('', 'utf-8')
            mock_dependencies['System'].return_value = system
            system.run_piped_output.return_value = process
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            zaproxy = MagicMock()
            BrowserkerScan(self.config, self.target, zaproxy).run()

        zaproxy.set_global_exclude_urls.assert_called_once_with(
            '^(?:(?!https?:\\/\\/(foo.com|bar.org|example.com)).*).$')

    def test_run_returns_scanned_urls(self):
        scanned_resources = [
            {'method': 'GET', 'type': 'url', 'url': 'https://example.com/1'},
            {'method': 'GET', 'type': 'url', 'url': 'https://example.com/2'},
        ]

        with patch.multiple('src.services.browserker_scan',
                            open=DEFAULT,
                            os=DEFAULT,
                            json=DEFAULT,
                            System=DEFAULT,
                            SecureReportParser=DEFAULT,
                            BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            parser = MagicMock()
            parser.parse_scanned_resources.return_value = scanned_resources
            mock_dependencies['SecureReportParser'].return_value = parser
            process.poll.return_value = 0
            process.stdout.readline.return_value = bytes('', 'utf-8')
            mock_dependencies['System'].return_value = system
            system.run_piped_output.return_value = process
            browserker = BrowserkerScan(self.config, self.target, MagicMock())
            browserker._set_zap_cookies = MagicMock()
            result = browserker.run()

        self.assertEqual(scanned_resources, result.scanned_resources)

    def test_run_sets_cookies(self):
        cookies_response = [
            {'name': 'name1', 'value': 'value1', 'domain': '', 'path': '', 'expires': 0,
             'size': 0, 'httpOnly': True, 'secure': True, 'session': True, 'priority': '',
             'time_observed': '2021-03-03T09:12:15.886752+09:00'},
            {'name': 'name2', 'value': 'value2', 'domain': '', 'path': '', 'expires': 0,
             'size': 0, 'httpOnly': True, 'secure': True, 'session': True, 'priority': '',
             'time_observed': '2021-03-03T09:12:15.886753+09:00'},
        ]

        with patch.multiple('src.services.browserker_scan',
                            open=DEFAULT,
                            os=DEFAULT,
                            json=DEFAULT,
                            System=DEFAULT,
                            BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            process.stdout.readline.return_value = bytes('', 'utf-8')
            mock_dependencies['System'].return_value = system
            system.run_piped_output.return_value = process
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=cookies_response)
            mock_zap = MagicMock()
            browserker = BrowserkerScan(self.config, self.target, mock_zap)
            browserker._get_browserker_urls = MagicMock()
            browserker.run()
            mock_zap.create_zap_httpsession.assert_called_with(cookies_response, self.target)

    def test_authenticate_starts_browserker_process(self):
        with patch.multiple('src.services.browserker_scan',
                            json=DEFAULT,
                            open=DEFAULT,
                            os=DEFAULT,
                            System=DEFAULT,
                            BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            process.stdout.readline.return_value = bytes('', 'utf-8')
            mock_dependencies['System'].return_value = system
            system.run_piped_output.return_value = process
            BrowserkerScan(self.config, self.target, MagicMock()).authenticate()

        mock_dependencies['BrowserkerConfigurationFile'].return_value.write.assert_called_once()
        system.run_piped_output.assert_called_once()

        self.assertIn('/browserker/analyzer', system.run_piped_output.mock_calls[0][1][0])
        self.assertIn('auth', system.run_piped_output.mock_calls[0][1][0])

    def test_authenticate_sets_cookies(self):
        cookies_response = [
            {'name': 'name1', 'value': 'value1', 'domain': '', 'path': '', 'expires': 0,
             'size': 0, 'httpOnly': True, 'secure': True, 'session': True, 'priority': '',
             'time_observed': '2021-03-03T09:12:15.886752+09:00'},
        ]

        with patch.multiple('src.services.browserker_scan',
                            open=DEFAULT,
                            os=DEFAULT,
                            json=DEFAULT,
                            System=DEFAULT,
                            BrowserkerConfigurationFile=DEFAULT) as mock_dependencies:
            system = MagicMock()
            process = MagicMock()
            process.poll.return_value = 0
            process.stdout.readline.return_value = bytes('', 'utf-8')
            mock_dependencies['System'].return_value = system
            system.run_piped_output.return_value = process
            mock_path = mock_dependencies['os']
            mock_path.path.isfile = MagicMock(return_value=True)
            mock_json = mock_dependencies['json']
            mock_json.load = MagicMock(return_value=cookies_response)
            mock_zap = MagicMock()
            browserker = BrowserkerScan(self.config, self.target, mock_zap)
            browserker.authenticate()
            mock_zap.create_zap_httpsession.assert_called_with(cookies_response, self.target)

from unittest import TestCase
from unittest.mock import DEFAULT, mock_open, patch

from src.services.browserker import BrowserkerConfigurationFile
from src.zap_gateway import Settings
from test.unit.mock_config import ToConfig


class TestBrowserkerConfigurationFile(TestCase):

    def test_writes_configuration_file(self):
        config = ToConfig(target='http://my.site.com:3000/context/page.html',
                          zap_port=9000,
                          exclude_urls=[],
                          paths_to_scan_list=['path1', 'path2'],
                          paths_to_scan_file_path='paths.txt',
                          browserker_allowed_hosts=['my.site.com'],
                          browserker_excluded_hosts=[],
                          browserker_ignored_hosts=[],
                          browserker_max_actions=20000,
                          browserker_max_attack_failures=2,
                          browserker_max_depth=4,
                          browserker_number_of_browsers=7,
                          browserker_navigation_timeout='10s',
                          browserker_page_loading_selector='css:#page-loading',
                          browserker_page_ready_selector='css:#page-ready',
                          browserker_secure_report_file='/output/secure-findings.json',
                          browserker_active_check_workers=5,
                          browserker_passive_check_workers=5,
                          browserker_crawl_timeout='5m')
        output = self._build_file(config)

        self.assertIn('AllowedHosts = ["my.site.com"]', output)
        self.assertIn('DataPath = "/data/browserker"', output)
        self.assertIn('DirectPaths = ["path1","path2"]', output)
        self.assertIn('DirectPathsFilePath = "paths.txt"', output)
        self.assertIn('ShowBrowser = false', output)
        self.assertIn('ExcludedHosts = []', output)
        self.assertIn('ExcludedURIs = []', output)
        self.assertIn('IgnoredHosts = []', output)
        self.assertIn('ExcludedElements = []', output)
        self.assertIn('LogLevel = "info"', output)
        self.assertIn('MaxActions = 20000', output)
        self.assertIn('MaxDepth = 4', output)
        self.assertIn('NumBrowsers = 7', output)
        self.assertIn('ScanMode = "passive"', output)
        self.assertIn('PluginResourcePath = "/browserker/resources/"', output)
        if config.zap_enabled:
            self.assertIn('Proxy = "http://127.0.0.1:9000"', output)
        self.assertIn('URL = "http://my.site.com:3000/context/page.html"', output)
        self.assertIn('NavigationTimeout = "10s"', output)
        self.assertIn('PageLoadingSelector = "css:#page-loading"', output)
        self.assertIn('PageReadySelector = "css:#page-ready"', output)
        self.assertIn('[FileLogLevels]', output)
        self.assertIn('LogLevel = "debug"', output)
        self.assertIn('[ConsoleLogLevels]', output)
        self.assertIn('LogLevel = "info"', output)
        self.assertIn('SecureReport = "/output/secure-findings.json"', output)
        self.assertNotIn('[DevToolsLogging]', output)
        self.assertIn('ActiveCheckWorkers = 5', output)
        self.assertIn('PassiveCheckWorkers = 5', output)
        self.assertIn('CrawlTimeout = "5m', output)

    def test_configures_excluded_urls(self):
        config = ToConfig(exclude_urls=['http://my.site/a', 'http://my.site/b/.*'])
        output = self._build_file(config)

        self.assertIn('ExcludedURIs = ["http://my.site/a", "http://my.site/b/.*"]', output)
        self.assertNotIn('ExcludedURIs = []', output)

    def test_configures_excluded_hosts(self):
        config = ToConfig(browserker_excluded_hosts=['site-a.com', '', 'site-b.com'])
        output = self._build_file(config)

        self.assertIn('ExcludedHosts = ["site-a.com", "site-b.com"]', output)

    def test_configures_allowed_hosts(self):
        config = ToConfig(browserker_allowed_hosts=['my.site.com', 'another-site.com'])
        output = self._build_file(config)

        self.assertIn('AllowedHosts = ["my.site.com", "another-site.com"]', output)

    def test_configures_ignored_hosts(self):
        config = ToConfig(browserker_ignored_hosts=['site-a.com', 'site-b.com'])
        output = self._build_file(config)

        self.assertIn('IgnoredHosts = ["site-a.com", "site-b.com"]', output)

    def test_configures_excluded_elements(self):
        config = ToConfig(browserker_excluded_elements=['css:.navigation-item'])
        output = self._build_file(config)

        self.assertIn('ExcludedElements = ["css:.navigation-item"]', output)

    def test_doesnt_exclude_authentication_page(self):
        config = ToConfig(exclude_urls=['http://my.site/login-page', 'http://my.site/large-download'],
                          auth_url='http://my.site/login-page')
        output = self._build_file(config)

        self.assertIn('ExcludedURIs = ["http://my.site/large-download"]', output)

    def test_configures_basic_digest_authentication(self):
        config = ToConfig(auth_url='http://my.site/login-page',
                          auth_username='usr', auth_password='pwd',
                          auth_type='basic-digest', exclude_urls=[])
        output = self._build_file(config)

        self.assertIn('[AuthDetails]', output)
        self.assertIn('UserName = "usr"', output)
        self.assertIn('Password = "pwd"', output)
        self.assertIn('AuthType = "basic-digest"', output)

    def test_configures_form_authentication(self):
        config = ToConfig(auth_url='http://my.site/login-page',
                          auth_username='usr', auth_password='pwd', exclude_urls=[])
        output = self._build_file(config)

        self.assertIn('[AuthDetails]', output)
        self.assertIn('UserName = "usr"', output)
        self.assertIn('Password = "pwd"', output)

    def test_configures_form_authentication_with_proper_escape_sequence(self):
        escape_seq = {
            '\H': '\\\\H',  # noqa: W605
            '\x01': '\\x01',
            '\\': '\\\\',
            }
        for seq, esc in escape_seq.items():
            config = ToConfig(auth_url='http://my.site/login-page',
                              auth_username=f'u{seq}sr', auth_password=f'p{seq}wd', exclude_urls=[])  # noqa: W605
            output = self._build_file(config)

            self.assertIn('[AuthDetails]', output)
            self.assertIn(f'UserName = "u{esc}sr"', output)  # noqa: W605
            self.assertIn(f'Password = "p{esc}wd"', output)  # noqa: W605

    def test_does_not_configure_auth_if_no_auth_url(self):
        config = ToConfig(auth_username='usr', auth_password='pwd', exclude_urls=[])
        output = self._build_file(config)

        self.assertNotIn('[AuthDetails]', output)
        self.assertNotIn('UserName = "usr"', output)
        self.assertNotIn('Password = "pwd"', output)
        self.assertNotIn('SubmitButtonField', output)

    def test_configures_form_authentication_fields(self):
        config = ToConfig(auth_username='usr', auth_password='pwd',
                          auth_url='http://my.site/login-page',
                          auth_username_field='[id=username]',
                          auth_first_submit_field='[id=continue]',
                          auth_password_field='[type=password]',
                          auth_submit_field='[id=submit]',
                          auth_verification_url='http://site.com',
                          auth_verification_selector='css:.home',
                          auth_path_to_login_form=['.login-form'],
                          auth_cookies=['session_id'],
                          auth_verification_login_form=True,
                          auth_report=True,
                          browserker_crawl_report=True,
                          browserker_crawl_graph=True,
                          exclude_urls=[],
                          browserker_always_relogin=True,
                          auth_disable_clear_fields=True)
        output = self._build_file(config)

        self.assertIn('[AuthDetails]', output)
        self.assertIn('UserName = "usr"', output)
        self.assertIn('Password = "pwd"', output)
        self.assertIn('UserNameField = "[id=username]"', output)
        self.assertIn('UserNameSubmitField = "[id=continue]"', output)
        self.assertIn('PasswordField = "[type=password]"', output)
        self.assertIn('VerificationURL = "http://site.com"', output)
        self.assertIn('VerificationSelector = "css:.home"', output)
        self.assertIn('VerificationLoginForm = true', output)
        self.assertIn('PathToLoginForm = [".login-form"]', output)
        self.assertIn('Cookies = ["session_id"]', output)
        self.assertIn('AlwaysRelogin = true', output)
        self.assertIn('ClearFields = false', output)
        self.assertIn(f'ReportPath = "{Settings.WRK_DIR}gl-dast-debug-auth-report.html"', output)
        self.assertIn(f'CrawlReportPath = "{Settings.WRK_DIR}gl-dast-debug-crawl-report.html"', output)
        self.assertIn(f'CrawlGraphImagePath = "{Settings.WRK_DIR}gl-dast-crawl-graph.svg"', output)

    def test_does_not_set_auth_clear_fields_when_not_set(self):
        config = ToConfig()
        output = self._build_file(config)
        self.assertNotIn('ClearFields = false', output)

    def test_defaults_submit_field_if_not_provided_and_other_fields_are(self):
        config = ToConfig(auth_username='usr', auth_password='pwd',
                          auth_url='http://my.site/login-page',
                          auth_username_field='[id=username]',
                          auth_password_field='[type=password]')
        output = self._build_file(config)

        self.assertIn('[AuthDetails]', output)
        self.assertIn('UserNameField = "[id=username]"', output)
        self.assertIn('PasswordField = "[type=password]"', output)
        self.assertIn('SubmitButtonField = "css:[type=submit], button"', output)

    def test_configures_custom_request_headers(self):
        config = ToConfig(request_headers={'Accept': 'application/json', 'Cache-Control': 'no-cache'})
        output = self._build_file(config)

        self.assertIn('[CustomHeaders]', output)
        self.assertIn('Accept = "application/json"', output)
        self.assertIn('Cache-Control = "no-cache"', output)

    def test_includes_dast_header_when_advertizing_scan(self):
        config = ToConfig(request_headers={}, advertise_scan=True)
        output = self._build_file(config)

        self.assertIn('[CustomHeaders]', output)
        self.assertIn('Via-Scanner = "Browserker"', output)

    def test_excludes_dast_header_when_not_advertizing_scan(self):
        config = ToConfig(request_headers={}, advertise_scan=False)
        output = self._build_file(config)

        self.assertNotIn('[CustomHeaders]', output)
        self.assertNotIn('Via-Scanner', output)

    def test_configures_custom_cookies(self):
        config = ToConfig(browserker_cookies={'name': 'fred', 'sessionid': '123456789'})
        output = self._build_file(config)

        self.assertIn('[CustomCookies]', output)
        self.assertIn('name = "fred"', output)
        self.assertIn('sessionid = "123456789"', output)

    def test_configures_console_log_levels(self):
        config = ToConfig(browserker_log={'brows': 'info', 'auth': 'debug'})
        output = self._build_file(config)

        self.assertIn('[ConsoleLogLevels]', output)
        self.assertIn('BROWS = "info"', output)
        self.assertIn('AUTH = "debug"', output)

    def test_configures_default_log_levels(self):
        config = ToConfig(browserker_log={'loglevel': 'trace', 'auth': 'debug'})
        output = self._build_file(config)

        self.assertIn('[ConsoleLogLevels]', output)
        self.assertIn('LogLevel = "trace"', output)
        self.assertIn('AUTH = "debug"', output)

    def test_configures_file_log_levels(self):
        config = ToConfig(browserker_file_log={'brows': 'info', 'auth': 'debug', 'chrome': 'trace'})
        output = self._build_file(config)

        self.assertIn('[FileLogLevels]', output)
        self.assertIn('BROWS = "info"', output)
        self.assertIn('AUTH = "debug"', output)
        self.assertIn('CHROME = "trace"', output)

    def test_configures_devtools_log_levels(self):
        config = ToConfig(browserker_devtools_log={'Default': 'messageAndBody,truncate:2000',
                                                   'Network': 'suppress',
                                                   'DOM.getDocument': 'message'})
        output = self._build_file(config)

        self.assertIn('[DevToolsLogging]', output)
        self.assertIn('"Default" = "messageAndBody,truncate:2000"', output)
        self.assertIn('"Network" = "suppress"', output)
        self.assertIn('"DOM.getDocument" = "message"', output)

    def test_does_not_configure_max_response_size_if_zero(self):
        config = ToConfig(browserker_max_response_size_mb=0)
        output = self._build_file(config)

        self.assertNotIn('MaxResponseSizeMB', output)

    def test_does_configures_max_response_size(self):
        config = ToConfig(browserker_max_response_size_mb=10)
        output = self._build_file(config)

        self.assertIn('MaxResponseSizeMB = 10', output)

    def test_does_not_configure_chrome_debug_log_dir_if_empty(self):
        config = ToConfig(browserker_chrome_debug_log_dir='')
        output = self._build_file(config)

        self.assertNotIn('ChromeDebugLogDir', output)

    def test_does_configure_chrome_debug_log_dir(self):
        config = ToConfig(browserker_chrome_debug_log_dir='/path')
        output = self._build_file(config)

        self.assertIn('ChromeDebugLogDir = "/path"', output)

    def test_does_not_enable_cache_if_disabled(self):
        config = ToConfig(browserker_cache=False)
        output = self._build_file(config)

        self.assertIn('DisableCache = true', output)

    def test_does_enable_cache_if_enabled(self):
        config = ToConfig(browserker_cache=True)
        output = self._build_file(config)

        self.assertIn('DisableCache = false', output)

    def test_does_not_enable_chromium_output_log_if_disabled(self):
        config = ToConfig(browserker_log_chromium_output=False)
        output = self._build_file(config)

        self.assertIn('LogChromiumProcessOutput = false', output)

    def test_does_enable_chromium_output_log(self):
        config = ToConfig(browserker_log_chromium_output=True)
        output = self._build_file(config)

        self.assertIn('LogChromiumProcessOutput = true', output)

    def test_configures_custom_hash_attributes(self):
        config = ToConfig(browserker_hash_attributes=['data-id', 'data-name'])
        output = self._build_file(config)

        self.assertIn('CustomHashAttributes = ["data-id", "data-name"]', output)

    def test_prints_settings_to_log(self):
        with patch.multiple(
            'src.services.browserker.browserker_configuration_file',
            logging=DEFAULT,
            open=mock_open(),
        ) as mocks:
            config = ToConfig(target='http://my.site.com/')
            BrowserkerConfigurationFile(config, '/tmp/config.toml', '/output/secure-findings.json').write()

            mocks['logging'].info.assert_any_call('Adding Browserker setting URL = "http://my.site.com/"')

    def test_redacts_some_settings_when_printing_to_log(self):
        with patch.multiple(
                'src.services.browserker.browserker_configuration_file',
                logging=DEFAULT,
                open=mock_open(),
        ) as mocks:
            config = ToConfig(
                auth_password='pwd',
                auth_url='http://my.site/login-page',
                auth_username='usr',
                pkcs12_cert=b'cert',
                pkcs12_password='dast',
                request_headers={'Accept': 'application/json', 'Cache-Control': 'no-cache'},
                target='http://my.site.com/',
            )

            BrowserkerConfigurationFile(config, '/tmp/config.toml', '/output/secure-findings.json').write()

            info_calls = [call[1][0] for call in mocks['logging'].info.mock_calls]
            self.assertIn('Adding Browserker setting [AuthDetails]', info_calls)
            self.assertIn('Adding Browserker setting Password = ********', info_calls)
            self.assertIn('Adding Browserker setting UserName = ********', info_calls)
            self.assertIn('Adding Browserker setting [CustomHeaders]', info_calls)
            self.assertIn('Adding Browserker setting Accept = ********', info_calls)
            self.assertIn('Adding Browserker setting Cache-Control = ********', info_calls)
            self.assertIn('Adding Browserker setting ClientCertificate = ********', info_calls)
            self.assertIn('Adding Browserker setting ClientCertificatePassword = ********', info_calls)

    def test_does_not_configure_timeouts(self):
        config = ToConfig()
        output = self._build_file(config)

        self.assertNotIn('NavigationTimeout', output)
        self.assertNotIn('ActionTimeout', output)
        self.assertNotIn('StabilityTimeout', output)
        self.assertNotIn('WaitAfterNavigation', output)
        self.assertNotIn('WaitAfterAction', output)
        self.assertNotIn('SearchElementTimeout', output)
        self.assertNotIn('ExtractElementTimeout', output)
        self.assertNotIn('ElementTimeout', output)
        self.assertNotIn('ActiveScanTimeout', output)
        self.assertNotIn('DOMReadyAfterTimeout', output)

    def test_allows_configuration_of_timeouts(self):
        config = ToConfig(
            browserker_navigation_timeout='1s',
            browserker_action_timeout='2s',
            browserker_stability_timeout='3s',
            browserker_navigation_stability_timeout='4s',
            browserker_action_stability_timeout='5s',
            browserker_search_element_timeout='6s',
            browserker_extract_element_timeout='7s',
            browserker_element_timeout='8s',
            browserker_active_scan_timeout='4h',
            browserker_dom_ready_after_timeout='200ms',
        )
        output = self._build_file(config)

        self.assertIn('NavigationTimeout = "1s"', output)
        self.assertIn('ActionTimeout = "2s"', output)
        self.assertIn('StabilityTimeout = "3s"', output)
        self.assertIn('WaitAfterNavigation = "4s"', output)
        self.assertIn('WaitAfterAction = "5s"', output)
        self.assertIn('SearchElementTimeout = "6s"', output)
        self.assertIn('ExtractElementTimeout = "7s"', output)
        self.assertIn('ElementTimeout = "8s"', output)
        self.assertIn('ActiveScanTimeout = "4h"', output)
        self.assertIn('DOMReadyAfterTimeout = "200ms"', output)

    def test_set_scan_mode_to_active_when_full_scan(self):
        config = ToConfig(browserker_include_only_rules=[], full_scan=True)

        output = self._build_file(config)

        self.assertIn('ScanMode = "active"', output)

    def test_only_run_implemented_checks(self):
        config = ToConfig(browserker_include_only_rules=['16.1', '16.2', '16.3'])

        output = self._build_file(config)

        self.assertIn('OnlyIncludeChecks = ["16.1", "16.2", "16.3"]', output)

    def test_run_user_defined_checks_when_supplied(self):
        config = ToConfig(feature_flags={'enableBrowserBasedAttacks': False})

        output = self._build_file(config)

        checks = ('"16.1", "16.2", "16.3", "16.4", "16.5", "16.6", "16.7", "16.8", "16.9", "16.10", '
                  '"200.1", "209.1", "209.2", "287.1", "287.2", '
                  '"319.1", "352.1", "359.1", "359.2", '
                  '"548.1", "598.1", "598.2", "598.3", '
                  '"601.1", "614.1", "693.1", '
                  '"798.1", "798.2", "798.3", "798.4", "798.5", "798.6", "798.7", "798.8", "798.9", "798.10", '
                  '"798.11", "798.12", "798.13", "798.14", "798.15", "798.16", "798.17", "798.18", "798.19", '
                  '"798.20", "798.21", "798.22", "798.23", "798.24", "798.25", "798.26", "798.27", "798.28", '
                  '"798.29", "798.30", "798.31", "798.32", "798.33", "798.34", "798.35", "798.36", "798.37", '
                  '"798.38", "798.39", "798.40", "798.41", "798.42", "798.43", "798.44", "798.46", "798.47", '
                  '"798.48", "798.49", "798.50", "798.52", "798.53", "798.54", "798.55", "798.56", "798.57", '
                  '"798.58", "798.59", "798.60", "798.61", "798.62", "798.63", "798.64", "798.65", "798.66", '
                  '"798.67", "798.68", "798.69", "798.70", "798.72", "798.74", "798.75", "798.77", "798.78", '
                  '"798.80", "798.81", "798.82", "798.83", "798.84", "798.86", "798.87", "798.88", "798.89", '
                  '"798.90", "798.91", "798.92", "798.93", "798.94", "798.95", "798.96", "798.97", "798.98", '
                  '"798.99", "798.100", "798.101", "798.102", "798.103", "798.104", "798.105", "798.106", '
                  '"798.107", "798.108", "798.109", "798.110", "798.111", "798.112", "798.113", "798.114", '
                  '"798.115", "798.116", "798.117", "798.118", "798.119", "798.120", "798.121", "798.122", "798.123", '
                  '"798.124", "798.125", "798.126", "798.127", "798.128", '
                  '"829.1", "829.2", '
                  '"1004.1"')

        self.assertIn(f'OnlyIncludeChecks = [{checks}]', output)

    def test_run_browser_based_attacks_when_feature_flag_is_enabled(self):
        config = ToConfig(feature_flags={'enableBrowserBasedAttacks': True})

        output = self._build_file(config)
        self.assertRegex(output, r'OnlyIncludeChecks = \["16.1", .*, "22.1"')

    def test_run_alpha_browser_based_attacks_when_feature_flag_is_enabled(self):
        config = ToConfig(feature_flags={'enableBrowserBasedAlphaAttacks': True})

        output = self._build_file(config)
        self.assertRegex(output, r'OnlyIncludeChecks = \["16.1".*"22.1".*"78.1"')

    def test_configures_file_log_when_set(self):
        config = ToConfig(browserker_file_log_path='/zap/wrk/debug.log')
        output = self._build_file(config)

        self.assertIn('FileLogPath = "/zap/wrk/debug.log"', output)

    def test_does_not_configure_file_log_when_not_set(self):
        config = ToConfig(browserker_file_log_path='')
        output = self._build_file(config)

        self.assertNotIn('FileLogPath', output)

    def test_configures_secure_report_extra_info_when_set(self):
        config = ToConfig(browserker_secure_report_extra_info=True)
        output = self._build_file(config)

        self.assertIn('SecureReportExtraInfo = true', output)

    def test_does_not_configure_secure_report_extra_info_when_not_set(self):
        config = ToConfig(browserker_secure_report_extra_info=False)
        output = self._build_file(config)

        self.assertNotIn('SecureReportExtraInfo', output)

    def test_configures_request_error_report_when_set(self):
        config = ToConfig(browserker_log_request_error_report=True)
        output = self._build_file(config)

        self.assertIn('LogRequestErrorReport = true', output)

    def test_does_not_configure_request_error_report_when_not_set(self):
        config = ToConfig(browserker_log_request_error_report=False)
        output = self._build_file(config)

        self.assertNotIn('LogRequestErrorReport', output)

    def test_configures_client_certificate(self):
        config = ToConfig(pkcs12_cert=b'cert', pkcs12_password='dast')
        output = self._build_file(config)

        self.assertIn('ClientCertificate = "Y2VydA=="', output)
        self.assertIn('ClientCertificatePassword = "dast"', output)

    def test_configure_feature_flags(self):
        config = ToConfig(feature_flags={'optimizeRegexps': True, 'enableBrowserBasedAttacks': False})
        output = self._build_file(config)

        self.assertIn('[FeatureFlags]', output)
        self.assertIn('OptimizeRegexps = true', output)
        self.assertIn('EnableBrowserBasedAttacks = false', output)

    def test_dont_configure_feature_flags_when_not_set(self):
        config = ToConfig(feature_flags={})
        output = self._build_file(config)

        self.assertNotIn('[FeatureFlags]', output)

    def test_dont_use_zap_as_proxy_when_feature_flag_is_enabled(self):
        config = ToConfig(feature_flags={'bypassZap': True})
        output = self._build_file(config)

        self.assertNotIn('Proxy =', output)

    def test_dont_set_db_memory_table_size_when_not_configured(self):
        config = ToConfig(browserker_db_memory_table_size=0)
        output = self._build_file(config)

        self.assertNotIn('[DatabaseDetails]', output)
        self.assertNotIn('MemoryTableSize', output)

    def test_set_db_memory_table_size_when_not_configured(self):
        config = ToConfig(browserker_db_memory_table_size=1048576)
        output = self._build_file(config)

        self.assertIn('[DatabaseDetails]', output)
        self.assertIn('MemoryTableSize = 1048576', output)

    def test_does_not_set_active_passive_workers_when_not_configured(self):
        config = ToConfig(browserker_active_check_workers=None, browserker_passive_check_workers=None)
        output = self._build_file(config)

        self.assertNotIn('ActiveCheckWorkers', output)
        self.assertNotIn('PassiveCheckWorkers', output)

    def test_does_not_set_crawl_timeout_when_not_configured(self):
        config = ToConfig(browserker_crawl_timeout=None)
        output = self._build_file(config)

        self.assertNotIn('CrawlTimeout', output)

    def test_configures_fips_mode_when_fips(self):
        config = ToConfig(fips_mode=True)
        output = self._build_file(config)

        self.assertIn('Proxy = "http://127.0.0.1:3128"', output)
        self.assertIn('FipsMode = true', output)

    def test_does_not_configure_fips_mode_when_not_fips(self):
        config = ToConfig(fips_mode=False)
        output = self._build_file(config)

        self.assertNotIn('Proxy = "http://127.0.0.1:3128"', output)
        self.assertNotIn('FipsMode = true', output)

    def _build_file(self, config) -> str:
        with patch('src.services.browserker.browserker_configuration_file.open', mock_open()) as handle_builder:
            BrowserkerConfigurationFile(config, '/tmp/config.toml', '/output/secure-findings.json').write()

        mock_handle = handle_builder()
        return ''.join([call[1][0] for call in mock_handle.write.mock_calls])

from unittest import TestCase

from src.config import BrowserkerActiveCheck, BrowserkerActiveChecks


class TestBrowserkerActiveChecks(TestCase):

    def test_returns_no_active_checks_when_not_enabled(self):
        feature_flags = {'enableBas': False, 'enableBrowserBasedAlphaAttacks': False,
                         'enableBrowserBasedAttacks': False}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('55.4', '3', False)])
        self.assertEqual([], checks.enabled_checks(feature_flags))

    def test_returns_active_checks_when_enabled(self):
        feature_flags = {'enableBas': False, 'enableBrowserBasedAlphaAttacks': False,
                         'enableBrowserBasedAttacks': True}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('55.4', '3', False),
                                         BrowserkerActiveCheck('22.1', '6', False),
                                         BrowserkerActiveCheck('78.1', '90020', True)])
        self.assertEqual(['22.1', '55.4'], checks.enabled_checks(feature_flags))

    def test_returns_alpha_active_checks_when_enabled(self):
        feature_flags = {'enableBas': False, 'enableBrowserBasedAlphaAttacks': True,
                         'enableBrowserBasedAttacks': False}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('99.4', '3', False),
                                         BrowserkerActiveCheck('22.1', '6', False),
                                         BrowserkerActiveCheck('78.1', '90020', True)])
        self.assertEqual(['22.1', '78.1', '99.4'], checks.enabled_checks(feature_flags))

    def test_returns_all_ids_when_attacks_and_alpha_attacks_enabled(self):
        feature_flags = {'enableBas': False, 'enableBrowserBasedAlphaAttacks': True,
                         'enableBrowserBasedAttacks': True}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('99.4', '3', False),
                                         BrowserkerActiveCheck('22.1', '6', False),
                                         BrowserkerActiveCheck('78.1', '90020', True)])
        self.assertEqual(['22.1', '78.1', '99.4'], checks.enabled_checks(feature_flags))

    def test_returns_bas_active_checks(self):
        feature_flags = {'enableBas': True, 'enableBrowserBasedAlphaAttacks': False,
                         'enableBrowserBasedAttacks': False}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('94.4', '3', False, ['94.4.2', '94.4.3']),
                                         BrowserkerActiveCheck('611.1', '6', False, ['611.1.5']),
                                         BrowserkerActiveCheck('78.1', '90020', True)])
        self.assertEqual(['611.1.5', '94.4.2', '94.4.3'], checks.enabled_checks(feature_flags))

    def test_returns_bas_and_enabled_active_checks(self):
        feature_flags = {'enableBas': True, 'enableBrowserBasedAlphaAttacks': False,
                         'enableBrowserBasedAttacks': True}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('94.4', '3', False, ['94.4.2', '94.4.3']),
                                         BrowserkerActiveCheck('611.1', '6', False, ['611.1.5']),
                                         BrowserkerActiveCheck('78.1', '90020', True, ['78.1.2'])])
        self.assertEqual(['611.1', '78.1.2', '94.4'], checks.enabled_checks(feature_flags))

    def test_all_feature_flags_enabled_returns_all_ids(self):
        feature_flags = {'enableBas': True, 'enableBrowserBasedAlphaAttacks': True,
                         'enableBrowserBasedAttacks': True}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('94.4', '3', False, ['94.4.2', '94.4.3']),
                                         BrowserkerActiveCheck('611.1', '6', False, ['611.1.5']),
                                         BrowserkerActiveCheck('78.1', '90020', True)])
        self.assertEqual(['611.1', '78.1', '94.4'], checks.enabled_checks(feature_flags))

    def test_returns_replaced_zap_rules(self):
        feature_flags = {'enableBas': False, 'enableBrowserBasedAlphaAttacks': False,
                         'enableBrowserBasedAttacks': True}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('94.4', '3', False),
                                         BrowserkerActiveCheck('611.1', '6', False),
                                         BrowserkerActiveCheck('78.1', '90020', True)])
        self.assertEqual(['3', '6'], checks.replaced_zap_rules(feature_flags))

    def test_does_not_replace_zap_rules_for_bas_active_checks(self):
        feature_flags = {'enableBas': True, 'enableBrowserBasedAlphaAttacks': False,
                         'enableBrowserBasedAttacks': False}

        checks = BrowserkerActiveChecks([BrowserkerActiveCheck('94.4', '3', False, ['94.4.2', '94.4.3']),
                                         BrowserkerActiveCheck('611.1', '6', False, ['611.1.5']),
                                         BrowserkerActiveCheck('78.1', '90020', True)])
        self.assertEqual([], checks.replaced_zap_rules(feature_flags))

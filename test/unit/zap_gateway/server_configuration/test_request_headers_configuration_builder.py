from unittest import TestCase
from unittest.mock import MagicMock, patch

from src.zap_gateway.server_configuration import RequestHeadersConfigurationBuilder


class TestRequestHeadersConfigurationBuilder(TestCase):

    def test_should_add_request_headers(self):
        config = MagicMock(
            allowed_hosts=['gitlab.com'],
            api_specification='',
            request_headers={'Authorization': 'Bearer my.token'},
        )

        with patch('src.zap_gateway.server_configuration.request_headers_configuration_builder.System') as mock_system:
            mock_system.return_value.dast_version.return_value = '1.7.3'

            configuration = RequestHeadersConfigurationBuilder(config).build()

            normalized = ' '.join(configuration)
            self.assertIn('-config replacer.full_list(0).url=^(?:(?=https?:\\/\\/(gitlab\\.com)).*).$', normalized)
            self.assertIn('-config replacer.full_list(0).description=header_0', normalized)
            self.assertIn('-config replacer.full_list(0).enabled=true', normalized)
            self.assertIn('-config replacer.full_list(0).matchtype=REQ_HEADER', normalized)
            self.assertIn('-config replacer.full_list(0).matchstr=Authorization', normalized)
            self.assertIn('-config replacer.full_list(0).regex=false', normalized)
            self.assertIn('-config replacer.full_list(0).replacement=Bearer my.token', normalized)

    def test_should_add_redacted_request_headers(self):
        config = MagicMock(
            allowed_hosts=['gitlab.com'],
            api_specification='',
            request_headers={'Authorization': 'Bearer my.token'},
        )

        with patch('src.zap_gateway.server_configuration.request_headers_configuration_builder.System') as mock_system:
            mock_system.return_value.dast_version.return_value = '1.7.3'

            configuration = RequestHeadersConfigurationBuilder(config).build(redacted=True)

            normalized = ' '.join(configuration)
            self.assertIn('-config replacer.full_list(0).url=^(?:(?=https?:\\/\\/(gitlab\\.com)).*).$', normalized)
            self.assertIn('-config replacer.full_list(0).description=header_0', normalized)
            self.assertIn('-config replacer.full_list(0).enabled=true', normalized)
            self.assertIn('-config replacer.full_list(0).matchtype=REQ_HEADER', normalized)
            self.assertIn('-config replacer.full_list(0).matchstr=Authorization', normalized)
            self.assertIn('-config replacer.full_list(0).regex=false', normalized)
            self.assertIn('-config replacer.full_list(0).replacement=********', normalized)

    def test_should_not_add_request_headers(self):
        config = MagicMock(
            allowed_hosts=['gitlab.com'],
            api_specification='https://gitlab.com/api.json',
            request_headers={'Authorization': 'Bearer my.token'},
        )

        with patch('src.zap_gateway.server_configuration.request_headers_configuration_builder.System'):
            configuration = RequestHeadersConfigurationBuilder(config).build()

            self.assertNotIn('-config replacer.full_list(0).url=https://gitlab\\.com', ' '.join(configuration))

    def test_should_add_dast_via_header_when_advertising(self):
        config = MagicMock(target='https://gitlab.com', request_headers={}, advertise_scan=True)

        with patch('src.zap_gateway.server_configuration.request_headers_configuration_builder.System') as mock_system:
            mock_system.return_value.dast_version.return_value = '1.7.3'

            configuration = RequestHeadersConfigurationBuilder(config).build()

            normalized = ' '.join(configuration)
            self.assertIn('-config replacer.full_list(0).matchtype=REQ_HEADER', normalized)
            self.assertIn('-config replacer.full_list(0).matchstr=Via', normalized)
            self.assertIn('-config replacer.full_list(0).regex=false', normalized)
            self.assertIn('-config replacer.full_list(0).replacement=GitLab DAST/ZAP v1.7.3', normalized)

    def test_should_not_add_dast_via_header_when_not_advertising(self):
        config = MagicMock(target='https://gitlab.com', request_headers={}, advertise_scan=False)

        with patch('src.zap_gateway.server_configuration.request_headers_configuration_builder.System') as mock_system:
            mock_system.return_value.dast_version.return_value = '1.7.3'

            configuration = RequestHeadersConfigurationBuilder(config).build()

            normalized = ' '.join(configuration)
            self.assertNotIn('REQ_HEADER', normalized)

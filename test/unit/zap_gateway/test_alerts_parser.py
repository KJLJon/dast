from unittest import TestCase

from src.models.http import HttpMessages
from src.zap_gateway import AlertsParser
from test.unit import factories


class TestAlertsParser(TestCase):

    def setUp(self):
        self.message = {}

    def test_parse_adds_message_to_alert_if_present(self):
        alerts = [factories.zap_api.alert(messageId='1'),
                  factories.zap_api.alert(messageId='2')]
        messages = HttpMessages([factories.models.http.http_message(message_id='1'),
                                 factories.models.http.http_message(message_id='2')])

        alerts = AlertsParser().parse(alerts, messages)

        self.assertEqual(2, len(alerts))
        self.assertEqual('1', str(alerts[0].message.message_id))
        self.assertEqual('2', str(alerts[1].message.message_id))

    def test_parse_adds_no_message_to_alert_if_not_present(self):
        alerts = [factories.zap_api.alert(messageId='666')]
        alerts = AlertsParser().parse(alerts, HttpMessages())

        self.assertEqual(1, len(alerts))
        self.assertEqual(None, alerts[0].message)

    def test_parse_adds_no_message_to_alert_if_field_not_present(self):
        alerts = [factories.zap_api.alert(messageId='666')]
        del alerts[0]['messageId']
        alerts = AlertsParser().parse(alerts, HttpMessages())

        self.assertEqual(1, len(alerts))
        self.assertEqual(None, alerts[0].message)
